import math,cairo
import barcode
import csv
import array
import PIL
import sys
import os


from barcode.writer import ImageWriter

from PIL import Image
from resizeimage import resizeimage

def parse_csv(CSVfilename,shipmentID,LabelList) :
    skuFlag = 0
    SKUIndex = 0
    TitleIndex = 1
    ASINIndex = 2
    FNSKUIndex = 3
    ShippedIndex = 9
    MRPIndex = 10
    SKUDict = {}

    # with open("FBA15BWYFK0B.tsv") as fd:
    with open(CSVfilename) as fd:
        # rd = csv.reader(fd, delimiter="\t", quotechar='"')
        rd = csv.reader(fd, delimiter=',', quotechar='|')
        # print(rd)
        for row in rd:
            if row:
                # print(row)
                if (row[SKUIndex] == 'Shipment ID'):
                    print("================")
                    print(row[1])
                    shipmentID = row[1]
                    print ("Shipment ID: "+shipmentID)
                if ((row[SKUIndex].find("Merchant SKU") != -1)):
                    print("Matched Mearchant SKU")
                    if ((row[TitleIndex].find("Title") != -1) & (row[FNSKUIndex].find("FNSKU") != -1) & (
                        row[ShippedIndex].find("Shipped") != -1)):
                        if (len(row) > MRPIndex):
                            if ((row[MRPIndex].find("MRP") == -1)):
                                print(
                                "MRP is not added into default tsv file. Add one Col with title MRP next to Shipped")
                                return
                            else:
                                skuFlag = 1
                                continue
                        else:
                            print("MRP is not added into default tsv file. Add one Col with title MRP next to Shipped")
                            return

                if (skuFlag):
                    SKUDict = {'SKU': row[SKUIndex], 'Title': row[TitleIndex][:85], 'FNSKU': row[FNSKUIndex], 'Count': row[ShippedIndex], 'MRP': row[MRPIndex]}
                    #print(row)
                    for i in range(0, int(row[9])):
                        LabelList.append(SKUDict)

        #print(LabelList)

        if (skuFlag == 0):
            print("Input TSV File is not compatible")
            return
    return shipmentID

def generate_PDF(shipmentID,LabelList) :
    pdffileList = []

    #print("=======@@@@@=======")
    #print(shipmentID)
    mm2pixel = 12

    # refer http://support-sg.canon-asia.com/img/G0047105.png
    printable_area_top_margin = 3
    printable_area_left_margin = 6.4
    printable_area_bottom_margin = 5
    printable_area_right_margin = 6.3

    A4width = 210.058
    A4height = 296.926

    PrintableA4width = A4width - printable_area_left_margin - printable_area_right_margin
    PrintableA4height = A4height - printable_area_top_margin - printable_area_bottom_margin

    width, height = A4width * mm2pixel, A4height * mm2pixel
    top_margin = 12 * mm2pixel
    side_margin = 6 * mm2pixel
    vertical_pitch = 34 * mm2pixel
    horizontal_pitch = 67 * mm2pixel
    label_height = 34 * mm2pixel
    label_width = 64 * mm2pixel
    font_size = 5 * mm2pixel / 2
    font_size_small = 2.5 * mm2pixel / 2
    labelbox_top_margin = 6
    labelbox_left_margin = 5

    pdfPageCount = math.ceil(float(len(LabelList))/24)

    #print("LabelList Length: " + str(len(LabelList)))
    #print("pdfPageCount: " + str(pdfPageCount))
    pdfFileName = shipmentID+"-barcode.pdf"
    surface = cairo.PDFSurface(pdfFileName, width, height)
    ctx = cairo.Context(surface)
    ctx.set_source_rgb(1, 1, 1)
    for pageIndex in range (0,int(pdfPageCount)):
        #pdfFileName = shipmentID+"-page"+str(pageIndex)+".pdf"

        pdffileList.append(pdfFileName)
        #print("pdfFileName: "+pdfFileName)



        #ctx.copy_page()
        #ctx.new_path()
        #for i in range(0,int(pdfPageCount)-1) :
        #    ctx.copy_page()

        for row in range(0, 8):
            for col in range(0, 3):
                # Set Line width and Line Colour for Label Box
                ctx.set_line_width(0.1)
                ctx.set_source_rgb(0, 0, 0)

                if (len(LabelList) <= ((pageIndex) * 24 + (row * 3) + (col))):
                    continue

                # Don't generate Rectangle as it confuses
                # Rectangle for each Label
                ctx.rectangle(side_margin + col * horizontal_pitch - printable_area_left_margin,
                              top_margin + row * vertical_pitch - printable_area_top_margin, label_width, label_height)
                ctx.stroke()

                # Create Font setting Font size/Colour/Type
                ctx.set_source_rgb(0, 0, 0)
                ctx.select_font_face("Georgia", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
                ctx.set_font_size(font_size)

                # Write SKU#
                # X Axis: side_margin+col*horizontal_pitch+labelbox_left_margin + (Jugaad Maring) 1*mm2pixel
                # Y Axis: top_margin+row*vertical_pitch + (Label Box margin from top) labelbox_top_margin*mm2pixel + (Jugaad Maring) 1*mm2pixel
                ctx.move_to(side_margin + col * horizontal_pitch + labelbox_left_margin + 1 * mm2pixel,
                            top_margin + row * vertical_pitch + labelbox_top_margin * mm2pixel + 1 * mm2pixel);
                ctx.show_text("SKU: " + LabelList[(pageIndex) * 24 + (row * 3) + (col)]['SKU'])

                #print("==========%%=========")
                #print("pageIndex: "+str(pageIndex))
                #print("row: " + str(row))
                #print("col: " + str(col))

                #print((pageIndex)*24+(row*3)+(col))
                #print(LabelList[(pageIndex)*24+(row*3)+(col)]['SKU'])

                # Write MRP#
                ctx.move_to(side_margin + col * horizontal_pitch + labelbox_left_margin + label_width * 2 / 3 + 1 * mm2pixel,
                    top_margin + row * vertical_pitch + labelbox_top_margin * mm2pixel + 1 * mm2pixel);
                ctx.show_text("MRP: "+LabelList[(pageIndex)*24+(row*3)+(col)]['MRP'])

                # Write Product1
                # X Axis: side_margin+col*horizontal_pitch+labelbox_left_margin + (Jugaad Maring) 1*mm2pixel
                # Y Axis: top_margin + row * vertical_pitch + labelbox_top_margin * mm2pixel + (Additional margin from above line - 1.5 times font size) font_size*1.5
                ctx.move_to(side_margin + col * horizontal_pitch + labelbox_left_margin + 1 * mm2pixel,
                            top_margin + row * vertical_pitch + labelbox_top_margin * mm2pixel + font_size * 1.5);
                ctx.set_font_size(font_size_small)
                ctx.show_text(LabelList[(pageIndex) * 24 + (row * 3) + (col)]['Title'])

                ctx.set_font_size(font_size)
                # Generate barcode image png file by default Width: 523 Height:280 pixel
                code128 = barcode.get('code128', LabelList[(pageIndex) * 24 + (row * 3) + (col)]['FNSKU'], writer=ImageWriter())
                filename = code128.save('barcode')

                # GEt Image Height and Width
                im = Image.open('barcode.png')
                bcwidth, bcheight = im.size

                # Resize Barcode Image (Stretch it horizontally and copress it vertically)
                # X Size - int(basewidth*0.95) - New Image Size equal to 95% of Labe lWidth
                # Y Size - int(bcheight*0.95) - New Image shoule be equal to 95% of original Height (Jugaad reduction to avoif trimming from bottom)
                img = Image.open('barcode.png')
                img = img.resize((int(label_width * 0.95), int(bcheight * 0.95)), PIL.Image.ANTIALIAS)
                img.save('barcode.png')

                # Insert Image into Label
                # X Axis: side_margin + col * horizontal_pitch + (label_width-bcwidth) / 2 (Get barcodewidth and Align it at centre of label width)
                # Y Axis: top_margin + row * vertical_pitch + label_height - bcheight
                image = cairo.ImageSurface.create_from_png("barcode.png")
                # ctx.set_source_surface(image, side_margin+col*horizontal_pitch+label_width/10, top_margin+row*vertical_pitch+label_height/3.2)
                # ctx.set_source_surface(image, side_margin + col * horizontal_pitch + (label_width-bcwidth) / 2,top_margin + row * vertical_pitch + label_height -bcheight)
                ctx.set_source_surface(image, side_margin + col * horizontal_pitch,
                                       top_margin + row * vertical_pitch + label_height - bcheight)
                ctx.paint()
        #ctx.copy_page()
        ctx.show_page()
    pdfPageCount
    print(pdffileList)
    #pdf_cat(pdffileList, "tmp.pdf")
    #merger("tmp.pdf",pdffileList)
    os.remove("barcode.png")

def main():
    shipmentID = ""
    LabelList = []

    if len(sys.argv) > 1:
        CSVfilename = sys.argv[1]
        # print(sys.argv[1])
    else:
        CSVfilename = "FBA15BWYFK0B.csv"

    shipmentID = parse_csv(CSVfilename, shipmentID, LabelList)

    if shipmentID is None:
        print("Looks like CSV Parsing failed. Can not generate PDF label File")

    else:
        print("================")
        print ("Shipment ID: " + shipmentID)
        generate_PDF(shipmentID, LabelList)


if __name__ == '__main__':
    main()