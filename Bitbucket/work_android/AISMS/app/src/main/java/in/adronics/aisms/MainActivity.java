package in.adronics.aisms;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;

public class MainActivity extends AppCompatActivity {

    BluetoothSPP bt;

    Button SettingBtn;
    Button DashboardBtn;
    Button AboutBtn;
    Button BlueSerialBtn;
    Button BSPPBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SettingBtn=(Button)findViewById(R.id.settingBtn);
        DashboardBtn=(Button)findViewById(R.id.dashboardBtn);
        AboutBtn=(Button)findViewById(R.id.aboutBtn);
        BlueSerialBtn=(Button)findViewById(R.id.bsBtn);
        BSPPBtn=(Button)findViewById(R.id.bsppBtn);

        AboutBtn.setOnLongClickListener(new View.OnLongClickListener() { 
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub
                //Toast.makeText(
                //        MainActivity.this,"About LongPressed",Toast.LENGTH_SHORT
                //).show();
                Intent intent = new Intent("in.adronics.aisms.BSPPMainActivity");
                startActivity(intent);

                return true;
            }
        });

        AboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(
                        MainActivity.this,"About Under Developemnt",Toast.LENGTH_SHORT
                ).show();
            }
        });

        SettingBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Toast.makeText(
                        MainActivity.this,"Setting Under Developemnt",Toast.LENGTH_SHORT
                ).show();

            }
        });


        DashboardBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Toast.makeText(
                        MainActivity.this,"Dashboard Under Development",Toast.LENGTH_SHORT
                ).show();

            }
        });

        BSPPBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Intent intent = new Intent("in.adronics.aisms.BSPPMainActivity");
                startActivity(intent);
            }
        });

        BlueSerialBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Intent intent = new Intent("in.adronics.aisms.BSHomescreen");
                startActivity(intent);

            }
        });

    }
}
